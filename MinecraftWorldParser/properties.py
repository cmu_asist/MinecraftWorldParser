"""
A set of functions to convert data nibbles extracted from Minecraft files to a
dictionary of properties, depending on the block type.
"""

import MinecraftElements


def getPropertiesNothing(nibble):
	"""
	Returns an empty dictionary for blocks with no properties of interest.
	"""

	return {}


def getPropertiesButton(nibble):
	"""
	Returns a dictionary of properties for a button, which consist of

	facing:  {NORTH, EAST, SOUTH, WEST, UP, DOWN}
	powered: {True, False}
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.DOWN,
			   1: MinecraftElements.Facing.EAST,
			   2: MinecraftElements.Facing.WEST,
			   3: MinecraftElements.Facing.SOUTH,
			   4: MinecraftElements.Facing.NORTH,
			   5: MinecraftElements.Facing.UP 
			 }

	properties['facing'] = FACING[nibble & 0x07]
	properties['powered'] = True if (nibble & 0x08) != 0 else False

	return properties


def getPropertiesDoor(nibble):
	"""
	Returns a dictionary of properties for a door, which consist of

	facing:  {NORTH, EAST, SOUTH, WEST}
	half:    {upper, lower}
	open:    {True, False}
	hinge:   {left, right}
	powered: {True, False}

	Note that only a partial set of properties are extracted, based on the half
	property of the door.  To get the complete set of properties, the upper and
	lower door half properties must be merged.
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.EAST,
			   1: MinecraftElements.Facing.SOUTH,
			   2: MinecraftElements.Facing.WEST,
			   3: MinecraftElements.Facing.NORTH,
			 }

	# The 0x08 bit indicates if it's an upper or lower part of the door
	# Note that properties are different for upper and lower doors
	properties['half'] = MinecraftElements.Half.upper if (nibble & 0x08) == 8 else MinecraftElements.Half.lower

	if properties['half'] == MinecraftElements.Half.lower:
		properties['facing'] = FACING[nibble & 0x03]
		properties['open'] = True if (nibble & 0x04) == 8 else False

	else:
		properties['hinge'] = [MinecraftElements.Hinge.left, MinecraftElements.Hinge.right][nibble & 0x01]
		properties['powered'] = True if (nibble & 0x02) == 4 else False

	return properties


def getPropertiesFence(nibble):
	"""
	Returns a dictionary of properties for fences, which consist of

	north: {True, False}
	south: {True, False}
	east:  {True, False}
	west:  {True, False}

	These are initially set to False, as the actual values of the properties
	need to be calculated during postprocessing.
	"""

	properties = { "north": False,
	               "south": False,
	               "east": False,
	               "west": False
	             }

	return properties



def getPropertiesGate(nibble):
	"""
	Returns a dictionary of properties for a fence gate, which consist of

	facing:  {NORTH, EAST, SOUTH, WEST}
	open:    {True, False}
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.SOUTH,
			   1: MinecraftElements.Facing.WEST,
			   2: MinecraftElements.Facing.NORTH,
			   3: MinecraftElements.Facing.EAST,
			 }

	properties['facing'] = FACING[nibble & 0x03]
	properties['open'] = True if (nibble & 0x04) == 4 else False

	return properties


def getPropertiesStairs(nibble):
	"""
	Returns a dictionary of properties for stair, which consist of

	facing:  {NORTH, EAST, SOUTH, WEST}
	half:    {top, bottom}
	shape:   {straight}

	Note that shape needs to be determined in a postprocessing step.
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.EAST,
	           1: MinecraftElements.Facing.WEST,
	           2: MinecraftElements.Facing.SOUTH,
	           3: MinecraftElements.Facing.NORTH
	         }

	properties['facing'] = FACING[nibble & 0x03]
	properties['half'] = MinecraftElements.Half.top if (nibble & 0x04) == 4 else MinecraftElements.Half.bottom

	# For now, let the shape of the stairs be straight.  Need to change this
	# in post-processing
	properties['shape'] = MinecraftElements.Shape.straight

	return properties


def getPropertiesPressurePlate(nibble):
	"""
	Returns a dictionary of properties for a pressure plate, which consist of

	powered: {True, False}
	"""

	properties = {}

	properties["powered"] = True if (nibble & 0x01) == 1 else False

	return properties



def getPropertiesWeightedPressurePlate(nibble):
	"""
	Returns a dictionary of properties for a powered pressure plate, 
	which consist of

	power:   [0,15]
	powered: {True, False}	

	Note that powered is not an explicit property of these types of blocks in
	Minecraft, but has been added as a convenience property to allow matching 
	with pressure plate blocks.
	"""

	properties = {}

	properties["power"] = nibble

	# This is added to be able to make a weighted pressure plate behave as a
	# pressur plate.
	properties["powered"] = properties["power"] > 0

	return properties


def getPropertiesSign(nibble):
	"""
	Returns a dictionary of properties for a button, which consist of

	facing:  { NORTH, EAST, SOUTH, WEST, 
	           NORTHEST, NORTHWEST, SOUTHEAST, SOUTHWEST,
	           SOUTH_SOUTHWEST, SOUTH_SOUTHEAST, 
	           NORTH_NORTHWEST, NORTH_NORTHEAST,
	           WEST_SOUTHWEST, WEST_NORTHWEST,
	           EAST_SOUTHEAST, EAST_NORTHEAST }
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.SOUTH,
	           1: MinecraftElements.Facing.SOUTH_SOUTHWEST,
	           2: MinecraftElements.Facing.SOUTHWEST,
	           3: MinecraftElements.Facing.WEST_SOUTHWEST,
	           4: MinecraftElements.Facing.WEST,
	           5: MinecraftElements.Facing.WEST_NORTHWEST,
	           6: MinecraftElements.Facing.NORTHWEST,
	           7: MinecraftElements.Facing.NORTH_NORTHWEST,
	           8: MinecraftElements.Facing.NORTH,
	           9: MinecraftElements.Facing.NORTH_NORTHEAST,
	           10: MinecraftElements.Facing.NORTHEAST,
	           11: MinecraftElements.Facing.EAST_NORTHEAST,
	           12: MinecraftElements.Facing.EAST,
	           13: MinecraftElements.Facing.EAST_SOUTHEAST,
	           14: MinecraftElements.Facing.SOUTHEAST,
	           15: MinecraftElements.Facing.SOUTH_SOUTHEAST
			 }

	properties["facing"] = FACING[nibble]

	return properties


def getPropertiesWallSign(nibble):
	"""	
	Returns a dictionary of properties for a wall sign, which consist of

	facing:  { NORTH, EAST, SOUTH, WEST }
	"""

	properties = {}

	FACING = { 2: MinecraftElements.Facing.NORTH,
			   3: MinecraftElements.Facing.SOUTH,
			   4: MinecraftElements.Facing.WEST,
			   5: MinecraftElements.Facing.EAST
			 }

	properties["facing"] = FACING[nibble & 0x07]

	return properties



def getPropertiesTrapdoor(nibble):
	"""
	Returns a dictionary of properties for a trap door, which consist of

	facing:  { NORTH, EAST, SOUTH, WEST }
	open:    { True, False }
	half:    { top, bottom }
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.SOUTH,
			   1: MinecraftElements.Facing.NORTH,
			   2: MinecraftElements.Facing.EAST,
			   3: MinecraftElements.Facing.WEST
			 }

	properties["facing"] = FACING[nibble & 0x03]
	properties["half"] = MinecraftElements.Half.top if (nibble & 0x04) == 4 else MinecraftElements.Half.bottom
	properties["open"] = (nibble & 0x08) == 8

	return properties


def getPropertiesSlab(nibble):
	"""
	Returns a dictionary of properties for a slab, which consist of

	half: { top, bottom }
	"""

	properties = {}

	properties["half"] = MinecraftElements.Half.top if (nibble & 0x8) == 8 else MinecraftElements.Half.bottom

	return properties



def getPropertiesTorch(nibble):
	"""
	Returns a dictionary of properties for a torch, which consist of

	facing:  { NORTH, EAST, SOUTH, WEST, UP }
	"""

	properties = {}

	FACING = { 1: MinecraftElements.Facing.EAST,
			   2: MinecraftElements.Facing.WEST,
			   3: MinecraftElements.Facing.SOUTH,
			   4: MinecraftElements.Facing.NORTH,
			   5: MinecraftElements.Facing.UP
			 }

	properties["facing"] = FACING[nibble & 0x07]

	return properties


def getPropertiesSnowLayer(nibble):
	"""
	Returns a dictionary of properties for a snow layer, which consist of

	layers: [1,8]
	"""

	properties = {}

	properties["layers"] = nibble & 0x07 + 1

	return properties


def getPropertiesLadder(nibble):
	"""
	Returns a dictionary of properties for a ladder, which consist of

	facing:  {NORTH, EAST, SOUTH, WEST}
	"""

	properties = {}

	FACING = { 2: MinecraftElements.Facing.NORTH,
			   3: MinecraftElements.Facing.SOUTH,
			   4: MinecraftElements.Facing.WEST,
			   5: MinecraftElements.Facing.EAST
	         }

	properties["facing"] = FACING[nibble & 0x07]

	return properties


def getPropertiesLever(nibble):
	"""
	Returns a dictionary of properties for a lever, which consist of

	facing:  {NORTH, EAST, SOUTH, WEST, UP_X, UP_Z, DOWN_X, DOWN_Z}
	powered: {True, False}
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.DOWN_X,
			   1: MinecraftElements.Facing.EAST,
			   2: MinecraftElements.Facing.WEST,
			   3: MinecraftElements.Facing.SOUTH,
			   4: MinecraftElements.Facing.NORTH,
			   5: MinecraftElements.Facing.UP_Z,
			   6: MinecraftElements.Facing.UP_X,
			   7: MinecraftElements.Facing.DOWN_Z
		     }

	properties["facing"] = FACING[nibble & 0x07]
	properties["powered"] = True if (nibble & 0x08) == 8 else False

	return properties


def getPropertiesBars(nibble):
	"""
	Returns a dictionary of properties for iron bars, which consist of

	north: {True, False}
	south: {True, False}
	east:  {True, False}
	west:  {True, False}

	These are initially set to False, as the actual values of the properties
	need to be calculated during postprocessing.
	"""

	properties = { "north": False,
	               "south": False,
	               "east": False,
	               "west": False
	             }

	return properties



def getPropertiesGlassPane(nibble):
	"""
	Returns a dictionary of properties for glass panes, which consist of

	north: {True, False}
	south: {True, False}
	east:  {True, False}
	west:  {True, False}

	These are initially set to False, as the actual values of the properties
	need to be calculated during postprocessing.
	"""

	properties = { "north": False,
	               "south": False,
	               "east": False,
	               "west": False
	             }

	return properties



def getPropertiesTripwireHook(nibble):
	"""
	Returns a dictionary of properties for a tripwire hook, which consist of

	facing:   { NORTH, EAST, SOUTH, WEST }
	powered:  { True, False }	
	attached: { True, False }
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.SOUTH,
	           1: MinecraftElements.Facing.WEST,
	           2: MinecraftElements.Facing.NORTH,
	           3: MinecraftElements.Facing.EAST
	         }

	properties["facing"] = FACING[nibble & 0x03]
	properties["attached"] = True if (nibble & 0x04) == 4 else False
	properties["powered"] = True if (nibble & 0x08) == 8 else False

	return properties



def getPropertiesWall(nibble):
	"""
	Returns a dictionary of properties for walls, which consist of

	north: {True, False}
	south: {True, False}
	east:  {True, False}
	west:  {True, False}
	up:    {True, False}

	These are initially set to False, as the actual values of the properties
	need to be calculated during postprocessing.
	"""

	properties = { "north": False,
	               "south": False,
	               "east": False,
	               "west": False,
	               "up": False
	             }

	return properties



def getPropertiesAnvil(nibble):
	"""
	Returns a dictionary of properties for an anvil, which consist of

	facing:  {NORTH, EAST, SOUTH, WEST}
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.WEST,
			   1: MinecraftElements.Facing.NORTH,
			   2: MinecraftElements.Facing.EAST,
			   3: MinecraftElements.Facing.SOUTH
	         }
	
	properties["facing"] = FACING[nibble&0x03]

	return properties


def getPropertiesHopper(nibble):
	"""
	Returns a dictionary of properties for a hopper, which consist of

	facing:  {NORTH, EAST, SOUTH, WEST, DOWN}
	enabled: {True, False}
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.DOWN,
			   2: MinecraftElements.Facing.NORTH,
			   3: MinecraftElements.Facing.SOUTH,
			   4: MinecraftElements.Facing.WEST,
			   5: MinecraftElements.Facing.EAST
	         }

	properties["enabled"] = True if (nibble&0x08) == 8 else False
	properties["facing"] = FACING[nibble&0x07]

	return properties


def getPropertiesFluid(nibble):
	"""
	Returns a dictionary of the properties for lava, which consist of

	level:  [0,15]
	"""

	properties = {}

	# TODO: Check if this is correct!!!
	properties["level"] = nibble

	return properties


def getPropertiesBed(nibble):
	"""
	Returns a dictionary of the properties for a bed, which consist of

	facing:  { NORTH, EAST, SOUTH, WEST }
	half:    { head, foot }
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.SOUTH,
	           1: MinecraftElements.Facing.WEST,
	           2: MinecraftElements.Facing.NORTH,
	           3: MinecraftElements.Facing.EAST
	         }

	properties["facing"] = FACING[nibble&0x03]
	properties["half"] = MinecraftElements.Half.head if nibble&0x08 == 8 else MinecraftElements.Half.foot

	return properties


def getPropertiesRail(nibble):
	"""
	Returns a dictionary of the properties for a rail, which consist of

	facing:  { NORTH, EAST, UP_X, DOWN_X, UP_Z, DOWN_Z }
	powered: [ True, False ]
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.NORTH,
	           1: MinecraftElements.Facing.EAST,
	           2: MinecraftElements.Facing.UP_X,
	           3: MinecraftElements.Facing.DOWN_X,
	           4: MinecraftElements.Facing.UP_Z,
	           5: MinecraftElements.Facing.DOWN_Z,
	         }

	properties["facing"] = FACING[nibble & 0x07]
	properties["powered"] = True if nibble & 0x08 == 8 else False

	return properties


def getPropertiesWheat(nibble):
	"""
	Returns a dictionary of the properties for wheat, which consist of

	age:  [0, 7]
	"""

	properties = {}

	# TODO: Check if this is correct!!!
	properties["age"] = nibble & 0x07

	return properties



def getPropertiesReeds(nibble):
	"""
	Returns a dictionary of the properties for reeds, which consist of

	age:  [0, 15]
	"""

	properties = {}

	# TODO: Check if this is correct!!!
	properties["age"] = nibble

	return properties


def getPropertiesFarmland(nibble):
	"""
	Returns a dictionary of the properties for farmland, which consist of

	moisture:  [0, 7]
	"""

	properties = {}

	# TODO: Check if this is correct!!!
	properties["moisture"] = nibble & 0x07

	return properties


def getPropertiesRedstoneWire(nibble):
	"""
	Returns a dictionary of the properties for restone wire, which consist of

	power - [0,15]

	north -
	south - 
	east - 
	west -

	Note that north, south, east and west will initially be set to None, the
	actual values must be calculated after parsing surrounding blocks
	"""

	properties = {}

	properties["power"] = nibble & 0x07

	properties["north"] = None
	properties["south"] = None
	properties["east"] = None
	properties["west"] = None

	return properties


def getPropertiesCake(nibble):
	"""
	Return a dictionary of the properties of a cake, which consist of

	bites - [0 - 6]
	"""

	properties = {}

	# TODO: Check if this is correct!!!
	properties["bites"] = nibble & 0x07

	return properties


def getPropertiesVine(nibble):
	"""
	Returns a dictionary of the properties for vine, which consist of

	north - { True, False }
	south - { True, False }
	east -  { True, False }
	west -  { True, False }
	up -    { True, False }
	"""

	properties = {}

	# TODO: See if these are actually grabbed from the nibble
	properties["north"] = False
	properties["south"] = False
	properties["east"] = False
	properties["west"] = False
	properties["up"] = False

	return properties


def getPropertiesRepeater(nibble):
	"""
	Returns a dictionary of the properties for a repeater, which consits of

	facing - { NORTH, SOUTH, EAST, WEST }
	delay  - [ 1 -- 4 ]
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.NORTH,
	           1: MinecraftElements.Facing.EAST,
	           2: MinecraftElements.Facing.SOUTH,
	           3: MinecraftElements.Facing.WEST
	         }

	properties["facing"] = FACING[nibble & 0x03]
	properties["delay"] = ((nibble & 0x0c) >> 2) + 1

	return properties


def getPropertiesObserver(nibble):
	"""
	Returns a dictionary of the properties for an observer, which consits of

	facing -  { DOWN, UP, NORTH, SOUTH, EAST, WEST }
	powered - { True, False }
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.DOWN,
	           1: MinecraftElements.Facing.UP,
	           2: MinecraftElements.Facing.NORTH,
	           3: MinecraftElements.Facing.SOUTH,
	           4: MinecraftElements.Facing.WEST,
	           5: MinecraftElements.Facing.EAST
	         }

	properties["facing"] = FACING[nibble & 0x07]
	properties["powered"] = True if nibble & 0x08 == 8 else False

	return properties


def getPropertiesBrewingStand(nibble):
	"""
	Returns a dictionary of the properties for a brewing stand, which consits of

	has_bottle_0 - { True, False }
	has_bottle_1 - { True, False }
	has_bottle_2 - { True, False }
	"""

	properties = {}

	properties["has_bottle_0"] = True if nibble & 0x01 == 1 else False
	properties["has_bottle_1"] = True if nibble & 0x02 == 2 else False
	properties["has_bottle_2"] = True if nibble & 0x04 == 4 else False

	return properties


def getPropertiesDetector(nibble):
	"""
	Returns a dictionary of the properties for a detector, which consits of

	power - [0 - 15]
	"""

	properties = {}

	properties["power"] = nibble

	return properties



def getPropertiesComparator(nibble):
	"""
	Returns a dictionary of the properties for a comparator, which consits of

	facing -  { NORTH, SOUTH, EAST, WEST }
	mode -    { compare, subtract }
	powered - { True, False }
	"""

	properties = {}

	FACING = { 0: MinecraftElements.Facing.NORTH,
	           1: MinecraftElements.Facing.EAST,
	           2: MinecraftElements.Facing.SOUTH,
	           3: MinecraftElements.Facing.WEST
	         }

	properties["facing"] = FACING[nibble & 0x03]
	properties["mode"] = "compare" if nibble & 0x04 == 0 else "subtract"
	properties["powered"] = True if nibble & 0x08 == 8  else False

	return properties

"""
NOT_YET_KNOWN maps currently unimplemented blocks to getPropertiesNothing
"""
NOT_YET_KNOWN = getPropertiesNothing

"""
PropertiesMap maps a block type to a getProperties function
"""
PropertiesMap = {
	MinecraftElements.Block.air: getPropertiesNothing,
	MinecraftElements.Block.stone: getPropertiesNothing,
	MinecraftElements.Block.grass: getPropertiesNothing,
	MinecraftElements.Block.dirt: getPropertiesNothing,
	MinecraftElements.Block.cobblestone: getPropertiesNothing,
	MinecraftElements.Block.planks: getPropertiesNothing,
	MinecraftElements.Block.sapling: NOT_YET_KNOWN,
	MinecraftElements.Block.bedrock: getPropertiesNothing,
	MinecraftElements.Block.flowing_water: getPropertiesFluid,
	MinecraftElements.Block.water: getPropertiesFluid,
	MinecraftElements.Block.flowing_lava: getPropertiesFluid,
	MinecraftElements.Block.lava: getPropertiesFluid,
	MinecraftElements.Block.sand: getPropertiesNothing,
	MinecraftElements.Block.gravel: getPropertiesNothing,
	MinecraftElements.Block.gold_ore: getPropertiesNothing,
	MinecraftElements.Block.iron_ore: getPropertiesNothing,
	MinecraftElements.Block.coal_ore: getPropertiesNothing,
	MinecraftElements.Block.log: getPropertiesNothing,
	MinecraftElements.Block.leaves: getPropertiesNothing,
	MinecraftElements.Block.sponge: getPropertiesNothing,
	MinecraftElements.Block.glass: getPropertiesNothing,
	MinecraftElements.Block.lapis_ore: getPropertiesNothing,
	MinecraftElements.Block.lapis_block: getPropertiesNothing,
	MinecraftElements.Block.dispenser: getPropertiesNothing,             
	MinecraftElements.Block.sandstone: getPropertiesNothing,
	MinecraftElements.Block.noteblock: NOT_YET_KNOWN,
	MinecraftElements.Block.bed: getPropertiesBed,
	MinecraftElements.Block.golden_rail: getPropertiesRail,
	MinecraftElements.Block.detector_rail: getPropertiesRail,
	MinecraftElements.Block.sticky_piston: NOT_YET_KNOWN,		
	MinecraftElements.Block.web: getPropertiesNothing,
	MinecraftElements.Block.tallgrass: NOT_YET_KNOWN,
	MinecraftElements.Block.deadbush: getPropertiesNothing,
	MinecraftElements.Block.piston: NOT_YET_KNOWN,
	MinecraftElements.Block.piston_head: NOT_YET_KNOWN,
	MinecraftElements.Block.wool: getPropertiesNothing,                      
	MinecraftElements.Block.piston_extension: NOT_YET_KNOWN,
	MinecraftElements.Block.yellow_flower: NOT_YET_KNOWN,
	MinecraftElements.Block.red_flower: NOT_YET_KNOWN,
	MinecraftElements.Block.brown_mushroom: getPropertiesNothing,
	MinecraftElements.Block.red_mushroom: getPropertiesNothing,
	MinecraftElements.Block.gold_block: getPropertiesNothing,
	MinecraftElements.Block.iron_block: getPropertiesNothing,                  
	MinecraftElements.Block.double_stone_slab: getPropertiesNothing,         
	MinecraftElements.Block.stone_slab: getPropertiesSlab,
	MinecraftElements.Block.brick_block: getPropertiesNothing,
	MinecraftElements.Block.tnt: getPropertiesNothing,
	MinecraftElements.Block.bookshelf: getPropertiesNothing,                 
	MinecraftElements.Block.mossy_cobblestone: getPropertiesNothing,
	MinecraftElements.Block.obsidian: getPropertiesNothing,
	MinecraftElements.Block.torch: getPropertiesTorch,
	MinecraftElements.Block.fire: getPropertiesNothing,
	MinecraftElements.Block.mob_spawner: getPropertiesNothing,
	MinecraftElements.Block.oak_stairs: getPropertiesStairs,                   
	MinecraftElements.Block.chest: getPropertiesNothing,							
	MinecraftElements.Block.redstone_wire: getPropertiesRedstoneWire,			
	MinecraftElements.Block.diamond_ore: getPropertiesNothing,
	MinecraftElements.Block.diamond_block: getPropertiesNothing,
	MinecraftElements.Block.crafting_table: getPropertiesNothing,            
	MinecraftElements.Block.wheat: getPropertiesWheat,
	MinecraftElements.Block.farmland: getPropertiesFarmland,
	MinecraftElements.Block.furnace: getPropertiesNothing,
	MinecraftElements.Block.lit_furnace: getPropertiesNothing,
	MinecraftElements.Block.standing_sign: getPropertiesSign,        	
	MinecraftElements.Block.wooden_door: getPropertiesDoor,
	MinecraftElements.Block.ladder: getPropertiesLadder,                       
	MinecraftElements.Block.rail: getPropertiesRail,
	MinecraftElements.Block.stone_stairs: getPropertiesStairs,
	MinecraftElements.Block.wall_sign: getPropertiesWallSign,                 
	MinecraftElements.Block.lever: getPropertiesLever,
	MinecraftElements.Block.stone_pressure_plate: getPropertiesPressurePlate,
	MinecraftElements.Block.iron_door: getPropertiesDoor,
	MinecraftElements.Block.wooden_pressure_plate: getPropertiesPressurePlate,
	MinecraftElements.Block.redstone_ore: getPropertiesNothing,
	MinecraftElements.Block.lit_redstone_ore: getPropertiesNothing,
	MinecraftElements.Block.unlit_redstone_torch: getPropertiesTorch,   
	MinecraftElements.Block.redstone_torch: getPropertiesTorch,
	MinecraftElements.Block.stone_button: getPropertiesButton,
	MinecraftElements.Block.snow_layer: getPropertiesSnowLayer,
	MinecraftElements.Block.ice: getPropertiesNothing,
	MinecraftElements.Block.snow: getPropertiesNothing,
	MinecraftElements.Block.cactus: getPropertiesNothing,
	MinecraftElements.Block.clay: getPropertiesNothing,                 
	MinecraftElements.Block.reeds: getPropertiesReeds,
	MinecraftElements.Block.jukebox: NOT_YET_KNOWN,
	MinecraftElements.Block.fence: getPropertiesFence,
	MinecraftElements.Block.pumpkin: getPropertiesNothing,
	MinecraftElements.Block.netherrack: getPropertiesNothing,
	MinecraftElements.Block.soul_sand: getPropertiesNothing,
	MinecraftElements.Block.glowstone: getPropertiesNothing,
	MinecraftElements.Block.portal: NOT_YET_KNOWN,
	MinecraftElements.Block.lit_pumpkin: getPropertiesNothing,
	MinecraftElements.Block.cake: getPropertiesCake,
	MinecraftElements.Block.unpowered_repeater: getPropertiesRepeater,
	MinecraftElements.Block.powered_repeater: getPropertiesRepeater,
	MinecraftElements.Block.stained_glass: getPropertiesNothing,
	MinecraftElements.Block.trapdoor: getPropertiesTrapdoor,
	MinecraftElements.Block.monster_egg: getPropertiesNothing,
	MinecraftElements.Block.stonebrick: getPropertiesNothing,
	MinecraftElements.Block.brown_mushroom_block: getPropertiesNothing,
	MinecraftElements.Block.red_mushroom_block: getPropertiesNothing,
	MinecraftElements.Block.iron_bars: getPropertiesBars,
	MinecraftElements.Block.glass_pane: getPropertiesGlassPane,
	MinecraftElements.Block.melon_block: getPropertiesNothing,
	MinecraftElements.Block.pumpkin_stem: NOT_YET_KNOWN,
	MinecraftElements.Block.melon_stem: NOT_YET_KNOWN,
	MinecraftElements.Block.vine: getPropertiesVine,
	MinecraftElements.Block.fence_gate: getPropertiesGate,
	MinecraftElements.Block.brick_stairs: getPropertiesStairs,
	MinecraftElements.Block.stone_brick_stairs: getPropertiesStairs,
	MinecraftElements.Block.mycelium: getPropertiesNothing,
	MinecraftElements.Block.waterlily: getPropertiesNothing,
	MinecraftElements.Block.nether_brick: getPropertiesNothing,
	MinecraftElements.Block.nether_brick_fence: getPropertiesFence,
	MinecraftElements.Block.nether_brick_stairs: getPropertiesStairs,
	MinecraftElements.Block.nether_wart: getPropertiesNothing,
	MinecraftElements.Block.enchanting_table: getPropertiesNothing,
	MinecraftElements.Block.brewing_stand: NOT_YET_KNOWN,
	MinecraftElements.Block.cauldron: getPropertiesNothing,
	MinecraftElements.Block.end_portal: NOT_YET_KNOWN,
	MinecraftElements.Block.end_portal_frame: getPropertiesNothing,
	MinecraftElements.Block.end_stone: NOT_YET_KNOWN,
	MinecraftElements.Block.dragon_egg: NOT_YET_KNOWN,
	MinecraftElements.Block.redstone_lamp: getPropertiesNothing,
	MinecraftElements.Block.lit_redstone_lamp: getPropertiesNothing,
	MinecraftElements.Block.double_wooden_slab: getPropertiesNothing,
	MinecraftElements.Block.wooden_slab: getPropertiesSlab,
	MinecraftElements.Block.cocoa: NOT_YET_KNOWN,
	MinecraftElements.Block.sandstone_stairs: getPropertiesStairs,
	MinecraftElements.Block.emerald_ore: getPropertiesNothing,
	MinecraftElements.Block.ender_chest: getPropertiesNothing,
	MinecraftElements.Block.tripwire_hook: getPropertiesTripwireHook,
	MinecraftElements.Block.tripwire: NOT_YET_KNOWN,
	MinecraftElements.Block.emerald_block: getPropertiesNothing,
	MinecraftElements.Block.spruce_stairs: getPropertiesStairs,
	MinecraftElements.Block.birch_stairs: getPropertiesStairs,
	MinecraftElements.Block.jungle_stairs: getPropertiesStairs,
	MinecraftElements.Block.command_block: getPropertiesNothing,
	MinecraftElements.Block.beacon: getPropertiesNothing,
	MinecraftElements.Block.cobblestone_wall: getPropertiesWall,
	MinecraftElements.Block.flower_pot: getPropertiesNothing,
	MinecraftElements.Block.carrots: NOT_YET_KNOWN,
	MinecraftElements.Block.potatoes: NOT_YET_KNOWN,
	MinecraftElements.Block.wooden_button: getPropertiesButton,
	MinecraftElements.Block.skull: NOT_YET_KNOWN,
	MinecraftElements.Block.anvil: getPropertiesAnvil,
	MinecraftElements.Block.trapped_chest: getPropertiesNothing,
	MinecraftElements.Block.light_weighted_pressure_plate: getPropertiesWeightedPressurePlate,
	MinecraftElements.Block.heavy_weighted_pressure_plate: getPropertiesWeightedPressurePlate,
	MinecraftElements.Block.unpowered_comparator: getPropertiesComparator,
	MinecraftElements.Block.powered_comparator: getPropertiesComparator,
	MinecraftElements.Block.daylight_detector: getPropertiesDetector,
	MinecraftElements.Block.redstone_block: getPropertiesNothing,
	MinecraftElements.Block.quartz_ore: getPropertiesNothing,
	MinecraftElements.Block.hopper: getPropertiesHopper,
	MinecraftElements.Block.quartz_block: getPropertiesNothing,
	MinecraftElements.Block.quartz_stairs: getPropertiesStairs,
	MinecraftElements.Block.activator_rail: getPropertiesRail,
	MinecraftElements.Block.dropper: getPropertiesNothing,
	MinecraftElements.Block.stained_hardened_clay: getPropertiesNothing,
	MinecraftElements.Block.stained_glass_pane: getPropertiesGlassPane,
	MinecraftElements.Block.leaves2: getPropertiesNothing,
	MinecraftElements.Block.log2: getPropertiesNothing,
	MinecraftElements.Block.acacia_stairs: getPropertiesStairs,
	MinecraftElements.Block.dark_oak_stairs: getPropertiesStairs,
	MinecraftElements.Block.slime: getPropertiesNothing,
	MinecraftElements.Block.barrier: getPropertiesNothing,
	MinecraftElements.Block.iron_trapdoor: getPropertiesTrapdoor,
	MinecraftElements.Block.prismarine: getPropertiesNothing,
	MinecraftElements.Block.sea_lantern: getPropertiesNothing,
	MinecraftElements.Block.hay_block: NOT_YET_KNOWN,
	MinecraftElements.Block.carpet: NOT_YET_KNOWN,
	MinecraftElements.Block.hardened_clay: getPropertiesNothing,
	MinecraftElements.Block.coal_block: getPropertiesNothing,
	MinecraftElements.Block.packed_ice: NOT_YET_KNOWN,
	MinecraftElements.Block.double_plant: NOT_YET_KNOWN,
	MinecraftElements.Block.standing_banner: getPropertiesSign,
	MinecraftElements.Block.wall_banner: getPropertiesWallSign,
	MinecraftElements.Block.daylight_detector_inverted: getPropertiesDetector,
	MinecraftElements.Block.red_sandstone: getPropertiesNothing,
	MinecraftElements.Block.red_sandstone_stairs: getPropertiesStairs,
	MinecraftElements.Block.double_stone_slab2: getPropertiesNothing,
	MinecraftElements.Block.stone_slab2: getPropertiesSlab,
	MinecraftElements.Block.spruce_fence_gate: getPropertiesGate,
	MinecraftElements.Block.birch_fence_gate: getPropertiesGate,
	MinecraftElements.Block.jungle_fence_gate: getPropertiesGate,
	MinecraftElements.Block.dark_oak_fence_gate: getPropertiesGate,
	MinecraftElements.Block.acacia_fence_gate: getPropertiesGate,
	MinecraftElements.Block.spruce_fence: getPropertiesFence,
	MinecraftElements.Block.birch_fence: getPropertiesFence,
	MinecraftElements.Block.jungle_fence: getPropertiesFence,
	MinecraftElements.Block.dark_oak_fence: getPropertiesFence,
	MinecraftElements.Block.acacia_fence: getPropertiesFence,
	MinecraftElements.Block.spruce_door: getPropertiesDoor,
	MinecraftElements.Block.birch_door: getPropertiesDoor,
	MinecraftElements.Block.jungle_door: getPropertiesDoor,
	MinecraftElements.Block.acacia_door: getPropertiesDoor,
	MinecraftElements.Block.dark_oak_door: getPropertiesDoor,
	MinecraftElements.Block.end_rod: NOT_YET_KNOWN,
	MinecraftElements.Block.chorus_plant: NOT_YET_KNOWN,
	MinecraftElements.Block.chorus_flower: NOT_YET_KNOWN,
	MinecraftElements.Block.purpur_block: NOT_YET_KNOWN,
	MinecraftElements.Block.purpur_pillar: NOT_YET_KNOWN,
	MinecraftElements.Block.purpur_stairs: getPropertiesStairs,
	MinecraftElements.Block.purpur_double_slab: getPropertiesNothing,
	MinecraftElements.Block.purpur_slab: getPropertiesSlab,
	MinecraftElements.Block.end_bricks: NOT_YET_KNOWN,
	MinecraftElements.Block.beetroots: NOT_YET_KNOWN,
	MinecraftElements.Block.grass_path: NOT_YET_KNOWN,
	MinecraftElements.Block.end_gateway: NOT_YET_KNOWN,
	MinecraftElements.Block.repeating_command_block: getPropertiesNothing,
	MinecraftElements.Block.chain_command_block: NOT_YET_KNOWN,
	MinecraftElements.Block.frosted_ice: NOT_YET_KNOWN,
	MinecraftElements.Block.magma: NOT_YET_KNOWN,
	MinecraftElements.Block.nether_wart_block: getPropertiesNothing,
	MinecraftElements.Block.red_nether_brick: getPropertiesNothing,
	MinecraftElements.Block.bone_block: getPropertiesNothing,
	MinecraftElements.Block.structure_void: getPropertiesNothing,
	MinecraftElements.Block.observer: getPropertiesObserver,
	MinecraftElements.Block.white_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.orange_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.magenta_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.light_blue_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.yellow_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.lime_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.pink_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.gray_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.silver_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.cyan_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.purple_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.blue_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.brown_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.green_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.red_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.black_shulker_box: getPropertiesNothing,
	MinecraftElements.Block.white_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.orange_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.magenta_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.light_blue_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.yellow_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.lime_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.pink_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.gray_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.light_gray_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.cyan_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.purple_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.blue_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.brown_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.green_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.red_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.black_glazed_terracotta: getPropertiesNothing,
	MinecraftElements.Block.concrete: getPropertiesNothing,
	MinecraftElements.Block.concrete_powder: getPropertiesNothing,
	MinecraftElements.Block.structure_block: getPropertiesNothing,
}