"""
Extract blocks from a world file, with any additional block properties.  This
file contains the following classes

Block          - A simple class to store information about individual blocks
BlockFilter    - A class to determine which files / blocks to extract
BlockExtractor - The main class used to extract blocks
"""

import os
import sys

import mcworldlib as mc
import MinecraftElements
from .properties import PropertiesMap


class Block:
	def __init__(self, block_type, location, **kwargs):
		self.location = location
		self.type = block_type
		self.properties = {}
		self.nibble = 0x00



class BlockFilter:
	"""
	A simple class that can be used to indicate if a block, chunk, or region
	should be included in the list of blocks generated.
	"""

	def __init__(self, lower_bound = (-1e52, 0, -1e52),
		               upper_bound = (1e52, 255, 1e52),
		               blocksToIgnore = set([MinecraftElements.Block.air])):
		"""
		Create a filter.

		Args:
			lower_bound   - the lower boundary of blocks to filter.
			upper_bound   - the upper boundary of blocks to filter.
			blockToIgnore - a set of blocks to ignore when filtering.  By
			                default, only ignores 'air' blocks.
		"""

		self.lower_bound = lower_bound
		self.upper_bound = upper_bound
		self.blockTypesToIgnore = set(blocksToIgnore)


	def ignore(self, block_type):
		"""
		Add the block type to ignore.

		Args:
			block_type - an integer value or element of MinecraftElements.Block
		"""

		self.blockTypesToIgnore.add(block_type)


	def byType(self, block_type):
		"""
		Return true if this filter should allow the block by type.

		Args:
			block_type - an integer value or element of MinecraftElements.Block

		Returns:
			True  if the block type is not in the filter's ignore set 
		"""

		return block_type not in self.blockTypesToIgnore


	def byLocation(self, location):
		"""
		Return true if the location is within the lower and upper bounds

		Args:
			location - (x,y,z) triple

		Returns:
			True  if the location lies within the filter boundaries
			False otherwise
		"""

		return location[0] >= self.lower_bound[0] and location[0] <= self.upper_bound[0] and \
		       location[1] >= self.lower_bound[1] and location[1] <= self.upper_bound[1] and \
		       location[2] >= self.lower_bound[2] and location[2] <= self.upper_bound[2]


	def byRegion(self, rx, rz):
		"""
		Return true if any of the locations in the region lie within the x and
		z boundaries.

		Args:
			rx [int] - The x value of the region
			rz [int] - The z value of the region

		Returns:
			True  if any blocks in the chunk fall within the filter boundaries
			False otherwise
		"""

		# Calculate the minimum and maximum x and z values of blocks in the region
		x_min = rx * 512
		x_max = (rx + 1) * 512 - 1
		z_min = rz * 512
		z_max = (rz + 1) * 512 - 1

		return x_max >= self.lower_bound[0] and x_min <= self.upper_bound[0] and \
		       z_max >= self.lower_bound[2] and z_min <= self.upper_bound[2]


	def byChunk(self, cx, cz):
		"""
		Return true if any of the locations in the chunk lies within the cx 
		and cz boundaries.

		Args:
			cx [int] - The x value of the chunk
			cz [int] - The z value of the chunk

		Returns:
			True  if any blocks in the chunk fall within the filter boundaries
			False otherwise
		"""

		# Calculate the minimum and maximum x and z values of blocks in the chunk
		x_min = cx * 16
		x_max = (cx + 1) * 16 - 1
		z_min = cz * 16
		z_max = (cz + 1) * 16 - 1

		return x_max >= self.lower_bound[0] and x_min <= self.upper_bound[0] and \
		       z_max >= self.lower_bound[0] and z_min <= self.upper_bound[2]


	def bySection(self, sy):
		"""
		Return true if the section will lie within the bounds of the y axis
		"""

		# Calculate the minimum and maximum values of y in the section
		y_min = sy*16
		y_max = (sy + 1)*16 - 1

		return y_max >= self.lower_bound[1] and y_min <= self.upper_bound[1]

		      
class BlockExtractor:
	"""
	Class to extract and process blocks from a Minecraft world.
	"""

	def __init__(self, world_path, block_filter=BlockFilter()):
		"""
		Create a new instance of a BlockExtractor.

		Args:
			world_path - path to the Minecraft world file to extract blocks from
			block_filter - an instance of BlockFilter.
		"""

		self.world = mc.load(world_path)
		self.blocks = []

		self.filter = block_filter


	def position(self, cx, cy, cz, offset):
		"""
		Calculate the absolute position of a block from chunk coordinates

		Args:
			cx     - x value of the chunk
			cy     - y value of the chunk
			cz     - z value of the chunk
			offset - offset of the block in the chunk's data
		"""

		x = cx*16 + offset % 16
		y = cy*16 + int(offset/256)
		z = cz*16 + int(offset/16)%16

		return x,y,z


	def getBlocksFromChunk(self, chunk):
		"""
		Get all the blocks in the given chunk.

		Args:
			chunk - the chunk to extract blocks from.  chunk is expected to be
			        an instance of mcworldlib RegionChunk.
		"""

		cx, cz = chunk.world_pos

		for section in chunk['']['Level']['Sections']:

			cy = section['Y']

			if self.filter.bySection(cy):

				for offset in range(len(section['Blocks'])):
					# Get the block ID and calculate the location
					block_id = int(section['Blocks'][offset])
					if block_id < 0:
						block_id += 256

					location = self.position(cx,cy,cz,offset)

					# If this is a block of interest, get its information
					if self.filter.byType(block_id) and \
					   self.filter.byLocation(location):

						# Calculate the block data nibble.
						block_data = section['Data'][int(offset/2)]
						if offset % 2 == 1:
							block_nibble = (block_data & 0xf0) >> 4
						else:
							block_nibble = block_data & 0x0f

						# Determine block properties based on type and nibble
						block = Block(MinecraftElements.Block(block_id), location)

						block.nibble = block_nibble
						block.properties = PropertiesMap[block.type](block.nibble)

						self.blocks.append(block)


	def getBlocksFromRegion(self, region):
		"""
		Return all the blocks in a region.

		Args:
			region - The region to extract blocks from.  region is expected to
			         be an instance of mcworldlib RegionFile.
		"""

		for chunk in region.chunks:
			if self.filter.byChunk(chunk.world_pos[0], chunk.world_pos[1]):
				print("  Processing Chunk", chunk)
				self.getBlocksFromChunk(chunk)
			else:
				print("  Ignoring Chunk", chunk)



	def extract(self):
		"""
		Calculate all the blocks in the world.  The extracted blocks are 
		written to the 'blocks' attribute of the instance.  This will overwrite
		any previous values in the 'blocks' attribute.
		"""

		# Empty out anything that's been calculated before
		self.blocks = []

		for region in self.world.regions:
			# For some reason, None can pop up.  Not sure what to do with it,
			# so ignore if it happens
			if region is not None:
				rx, ry = region
				if self.filter.byRegion(rx, ry):
					print("Extracting Blocks from Region", (rx,ry))
					self.getBlocksFromRegion(self.world.regions[rx,ry])
				else:
					print("Ignoring Region at", (rx, ry))


