# MinecraftWorldParser

This module is used to extract individual blocks from a Minecraft save file.
The properties of blocks extracted are included, either directly from the save
files, or through post-processing steps.


## Usage

The script `worldParser.py` is the simplest approach to extract blocks from the
world file to store in a JSON format.  This can be run with the following
command

    python worldParser.py   INPUT_PATH   OUTPUT_PATH  -l LX LY LZ  -u UX UY UZ  -p

`INPUT_PATH` is the path to the directory containing the Minecraft save file.

`OUTPUT_PATH` is the path to the JSON file to write blocks to.

The `-l` and `-u` flags are optional flags used to impose bounds on the (x,y,z)
locations of blocks extracted.  In other words, if these arguments are passed
in, then only the blocks defined by locations `LX <= x <= UX`, `LY <= y <= UY`,
and `LZ <= z <= UZ` will be extracted from the world and written to the JSON
file.

The `-p` flag will result in pretty-printing the output json file.  Each 
instance of the `-p` flag will increment the amount of indentation by a space.
For example, `-ppp` or `-p -p -p` will result in an indentation of 3 spaces for
each level in the JSON file.  Is this flag isn't provided, then the JSON file
will be stored as flat.


## Some Handy Numbers

The bounds of the Sparky map: (-2168, 51, 152) to (-2108, 58, 199)

The bounds of the Falcon map: (-2127, 59, 143) to (-2020, 65, 192)

The bounds of the Competency Test map: (-2200, 28, 75) to (-2104, 32, 151)


## Requirements

This module utilizes the `mcworldlib` (https://www.github.com/MestreLion/mcworldlib)
module to extract data from the Anvil files in the Minecraft save files, and
the `MinecraftElements` (https://www.gitlab.com/cmu_asist/MinecraftElements)
module for enumerations of the blocks extracted.

The `mcworldlib` repo can be cloned via git, and installed with pip:

    git clone https://www.github.com/MestreLion/mcworldlib
    cd mcworldlib
    pip3 install --user -e .

The `MinecraftElements` repo can be installed in a similar manner.  Note that
the `develop` branch should (currently) be used, as it is the most up-to-date
version, and supports installation.

    git clone -b develop https://www.gitlab.com/cmu_asist/MinecrafElements
    cd MinecraftElements
    pip3 install --user -e .


## Acknowledgements

This module was build based on the `ASIST-MC-Toolbox`(https://www.github.com/zt-yang/ASIST-MC_Toolbox) 
written by Zhutian Yang and Paul Robertson (DOLL ASIST TA1 team).